import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Magazine {
    private Map<PRODUCT_CLASS, List<Product>> kolekcjaProduktow = new HashMap<>();
    private List<Product> lowClassList = new LinkedList<Product>();
    private List<Product> midClassList = new LinkedList<Product>();
    private List<Product> highClassList = new LinkedList<Product>();

    public void addProduct(String name, Double value, PRODUCT_CLASS pclass, PRODUCT_TYPE type){
        Product produkt = new Product(name,value,pclass,type);

        if (pclass.equals(PRODUCT_CLASS.HIGH)){
            highClassList.add(produkt);
            kolekcjaProduktow.put(pclass, highClassList);
        }
        else if (pclass.equals(PRODUCT_CLASS.MID)){
            midClassList.add(produkt);
            kolekcjaProduktow.put(pclass, midClassList);
        }
        else if (pclass.equals(PRODUCT_CLASS.LOW)){
            lowClassList.add(produkt);
            kolekcjaProduktow.put(pclass, lowClassList);
        }
        else{
            System.out.println("WRONG CLASS!");
        }
    }

    public List<Product> getListByClass(PRODUCT_CLASS pclass){
        return kolekcjaProduktow.get(pclass);
    }

    public boolean doesProductExist(String name){
        boolean checker = false;
        for (List<Product> productClassList : kolekcjaProduktow.values()){
            for (Product singleProduct : productClassList){
                if (singleProduct.getNazwaProduktu().equals(name)){
                    checker = true;
                }
            }
        }
        return checker;
    }

    public List<Product> getListByType(PRODUCT_TYPE type){
        List<Product> listaUporzadkowana = new LinkedList<>();
        for (List<Product> productClassList : kolekcjaProduktow.values()){
            for (Product singleProduct : productClassList){
                if (singleProduct.getTypProduktu() == type){
                    listaUporzadkowana.add(singleProduct);
                }
            }
        }
        return listaUporzadkowana;
    }

    public boolean isOnShelf(PRODUCT_CLASS polka, String name){
        boolean checker = false;
        for(List<Product> listOfProducts : kolekcjaProduktow.values()){
            for(Product singleProduct : listOfProducts){
                if (singleProduct.getNazwaProduktu().equals(name)){
                    if (singleProduct.getKlasaProduktu() == polka){
                        checker = true;
                    }
                }
            }
        }
        return checker;
    }
}