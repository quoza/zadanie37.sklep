public class Product {
    private String nazwaProduktu;
    private double cenaProduktu;
    private PRODUCT_CLASS klasaProduktu;
    private PRODUCT_TYPE typProduktu;

    public Product(String nazwaProduktu, double cenaProduktu, PRODUCT_CLASS klasaProduktu, PRODUCT_TYPE typProduktu) {
        this.nazwaProduktu = nazwaProduktu;
        this.cenaProduktu = cenaProduktu;
        this.klasaProduktu = klasaProduktu;
        this.typProduktu = typProduktu;
    }

    public String getNazwaProduktu() {
        return nazwaProduktu;
    }

    public void setNazwaProduktu(String nazwaProduktu) {
        this.nazwaProduktu = nazwaProduktu;
    }

    public double getCenaProduktu() {
        return cenaProduktu;
    }

    public void setCenaProduktu(double cenaProduktu) {
        this.cenaProduktu = cenaProduktu;
    }

    public PRODUCT_CLASS getKlasaProduktu() {
        return klasaProduktu;
    }

    public void setKlasaProduktu(PRODUCT_CLASS klasaProduktu) {
        this.klasaProduktu = klasaProduktu;
    }

    public PRODUCT_TYPE getTypProduktu() {
        return typProduktu;
    }

    public void setTypProduktu(PRODUCT_TYPE typProduktu) {
        this.typProduktu = typProduktu;
    }

    @Override
    public String toString() {
        return "Product {" +
                "nazwaProduktu = '" + nazwaProduktu + '\'' +
                ", cenaProduktu = " + cenaProduktu +
                ", klasaProduktu = " + klasaProduktu +
                ", typProduktu = " + typProduktu +
                '}';
    }
}
